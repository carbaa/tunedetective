const path = require("path");
const webpack = require("webpack");
const StylelintPlugin = require("stylelint-webpack-plugin");

module.exports = {
  entry: "./src/index.js",
  mode: "development",
  output: {
    path: path.resolve(__dirname, "public"),
    publicPath: "/public/",
    filename: "output.js",
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|.ts|.tsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: { presets: ["@babel/preset-env", "@babel/preset-react", "@babel/preset-typescript"] },
            //TODO: figure out if i need to add "@babel/transform-runtime" here as well
          },
        ],
      },
      {
        test: /\.css/,
        use: [{ loader: "css-loader" }, { loader: "style-loader" }],
      }
      /*{
        test: /\.s[ac]ss$/,
        use: [
          "style-loader",
          "css-loader",
          {
            loader: "sass-loader",
            options: { implementation: require("sass") },
          },
        ],
      },*/
    ],
  },
  devServer: {
    contentBase: path.join(__dirname, "public"),
    publicPath: "http://localhost:3000/",
    hotOnly: true,
    port: 3000,
  },
  plugins: [new webpack.HotModuleReplacementPlugin(), new StylelintPlugin(), new webpack.DefinePlugin({ API_ADDRESS: JSON.stringify(process.env.API_ADDRESS) })],
};
