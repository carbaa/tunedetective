import { hot } from "react-hot-loader/root";
import React from "react";
import { useSelector } from 'react-redux'

import { Toolbar } from "@material-ui/core";
import { searchSongs } from './redux/siteState'

import ResultsArea from "./components/ResultsArea"
import TuneDetectiveAppBar from "./components/TuneDetectiveAppBar";


const App = () => {
  const query = useSelector(store => store.siteState.query);

  return (<div className="app">
    <header>currentQuery
      <TuneDetectiveAppBar searchSongs={searchSongs} query={query} />
    </header>
    <Toolbar />
    <main>
      <ResultsArea />
    </main>
  </div >
  )

}

export default hot(App);
