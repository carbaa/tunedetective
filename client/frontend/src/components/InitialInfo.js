import React from "react";
import { Chip, makeStyles, Typography } from "@material-ui/core";

export default function InitialInfo() {
    const useStyles = makeStyles(theme => ({
        root: { '& > *': { margin: theme.spacing(0.5) } }
    }));

    const classes = useStyles();

    return (<div style={{ height: 'calc(0.9 * 100vh)', margin: '8px' }}>
        <Typography>There are also search options available</Typography>
        <Typography>Following options matches the following attributes exactly:</Typography>
        <div className={classes.root}>
            <Chip label="artist" />
            <Chip label="album" />
            <Chip label="title" />
        </div>

        <Typography>Example:</Typography>
        <div className={classes.root}>
            <Chip label="artist: Coldplay" />
        </div>

        <Typography>This demo uses <a href="http://millionsongdataset.com/">Million Song Dataset</a> </Typography>
    </div>);
}