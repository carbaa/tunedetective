import React from "react";
import { useSelector, useDispatch } from 'react-redux'

import { Paper, makeStyles, Grid, CardContent, Typography } from '@material-ui/core'
import Pagination from "@material-ui/lab/Pagination"
import CircularProgress from '@material-ui/core/CircularProgress'

import { searchSongs, updatePage } from '../redux/siteState'

import SearchResults from "./SearchResults"
import InitialInfo from "./InitialInfo"

export default function ResultsArea() {
    const results = useSelector(store => store.siteState.results);
    const currentPage = useSelector(store => store.siteState.page);
    const totalPages = useSelector(store => store.siteState.totalPages);
    const status = useSelector(store => store.siteState.status);
    const error = useSelector(store => store.siteState.error);

    const useStyles = makeStyles({
        topPaginationStyles: { padding: '15px 0' },
        bottomPaginationStyles: { padding: '15px 0' }
    });

    const dispatch = useDispatch();
    const classes = useStyles();

    const updatePageAndSearch = page => {
        if (page != currentPage) {
            dispatch(updatePage(page));
            dispatch(searchSongs());
        }
    }

    let ReturnElement = <div></div>;

    switch (status) {
        case 'initial':
            ReturnElement = <InitialInfo />;
            break;
        case 'loading':
            ReturnElement = <CircularProgress />
            break;
        case 'idle':
            ReturnElement = <>
                <Pagination
                    count={totalPages} page={currentPage} siblingCount={3} className={classes.topPaginationStyles} onChange={(event, page) => {
                        updatePageAndSearch(page);
                    }} />

                <SearchResults results={results} />

                <Pagination
                    count={totalPages} page={currentPage} siblingCount={3} className={classes.bottomPaginationStyles} onChange={(event, page) => {
                        updatePageAndSearch(page);
                    }} />
            </>
            break;
        case 'error':
            ReturnElement = <Grid container direction="column" spacing={2}>
                <Grid item xs={12}>
                    <CardContent>
                        <Typography variant="h6" component="h2">{error}</Typography>
                    </CardContent>
                </Grid>
            </Grid>
            break;
    }

    return (
        <Paper>
            {ReturnElement}
        </Paper>);
}