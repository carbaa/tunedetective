import React from "react";

import { Grid, Typography, CardContent } from '@material-ui/core'
import SearchItem from './SearchItem'

const SearchResults = ({ results }) =>
    <Grid container direction="column" spacing={2}>
        {results.length > 0 ?
            results.map(item => <SearchItem props={{ item }} />) :
            <Grid item xs={12}>
                <CardContent>
                    <Typography variant="h6" component="h2">Search returned no results</Typography>
                </CardContent>
            </Grid>}
    </Grid>


export default SearchResults;