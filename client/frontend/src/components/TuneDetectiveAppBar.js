import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Typography, AppBar, Toolbar, InputBase, makeStyles, fade } from "@material-ui/core";
import { Search as SearchIcon } from "@material-ui/icons"

import { updateQueryString, resetToInitial } from '../redux/siteState'

const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1,
    },
    brandName: {
        '&:hover': {
            cursor: 'pointer'
        }
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
            minWidth: '400px'
        },
        [theme.breakpoints.down('sm')]: {
            marginLeft: theme.spacing(3),
            width: '250px'
        }
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        width: '100%'
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
    }
}));

const TuneDetectiveAppBar = ({ searchSongs, query }) => {
    const classes = useStyles();

    const [currentQuery, setCurrentQuery] = useState(query);


    const dispatch = useDispatch();

    const onKeyDownHandler = e => {
        if (e.key == "Enter") {
            dispatch(updateQueryString(currentQuery));
            dispatch(searchSongs());
        }
    }

    return (<AppBar position="fixed">
        <Toolbar>
            <Typography variant="h6" onClick={() => { dispatch(resetToInitial()); }} className={classes.brandName}>TuneDetective</Typography>
            <div className={classes.search}>
                <div>
                    <SearchIcon className={classes.searchIcon} />
                </div>
                <InputBase
                    classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput
                    }}
                    placeholder={"artist: Coldplay"}
                    onChange={e => setCurrentQuery(e.target.value)}
                    onKeyDown={onKeyDownHandler}
                    value={currentQuery}
                />
            </div>
        </Toolbar>
    </AppBar>)

}
export default TuneDetectiveAppBar;