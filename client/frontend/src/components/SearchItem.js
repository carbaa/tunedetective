import React from "react";
import { Grid, Typography, Card, CardContent } from '@material-ui/core'

const SearchItem = ({ props }) => {
    const { item } = props;
    const { artist, title, album, length } = item;

    const lengthInNumbers = Number(length)
    const minutes = Math.floor(lengthInNumbers / 60);
    const seconds = Math.floor(lengthInNumbers - (60 * minutes))

    return (<Grid item xs={12}>
        <Card variant="outlined">
            <CardContent>
                <Typography variant="h6" component="h2">{artist} - {title}  </Typography>
                <Typography variant="subtitle1">Album: {album}</Typography>
                <Typography variant="subtitle1">Length: {minutes}:{seconds.toLocaleString(undefined, { minimumIntegerDigits: 2 })}</Typography>
            </CardContent>
        </Card>
    </Grid >)
}

export default SearchItem