import { configureStore } from '@reduxjs/toolkit'
import siteStateReducer from './redux/siteState'

// create a store
export default configureStore({
    reducer: {
        siteState: siteStateReducer
    }
})