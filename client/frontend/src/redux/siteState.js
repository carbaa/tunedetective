import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

export const searchSongs = createAsyncThunk('siteState/searchSongs',
    async (_, thunkAPI) => {
        const { getState, rejectWithValue } = thunkAPI;

        const { siteState } = getState();
        const { size, page, query, results } = siteState;

        // reject empty queries

        if (!query) {
            return rejectWithValue({ query, page, totalPages: null, results, error: "The query is empty" });
        }

        // if there's no page, then set the page to 1
        const pageToQuery = page ? page : 1;

        const from = (pageToQuery - 1) * size;

        try {
            const res = await fetch(API_ADDRESS,
                {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ query, from, size })
                }
            );

            const body = await res.json();

            if ('error' in body) {
                return rejectWithValue({ query, page: null, totalPages: null, results: [], error: body.error });
            }

            else {
                const totalPages = Math.ceil(body.total / body.size);
                return { query, page: pageToQuery, totalPages, results: body.results, error: null };
            }
        }

        catch {
            return rejectWithValue({ query, page: null, totalPages: null, results: [], error: "Failed to connect to the API" });
        }


    }
);

const makeInitialState = () => ({
    status: 'initial', //status: 'idle', 'error', 'updating'
    page: null,
    totalPages: null,
    size: 20,
    results: [],
    error: null,
    query: ''
});


const siteState = createSlice({
    name: 'siteState',
    initialState: makeInitialState(),
    reducers: {
        updateQueryString(state, action) {
            //Reset page number as well if the querystring is different
            if (state.query != action.payload)
                state.page = null;

            state.query = action.payload;
        },

        updateStatus(state, action) {
            state.status = action.payload;
        },

        updatePage(state, action) {
            state.page = action.payload;
        },

        resetToInitial(state) {
            //TODO: figure out why i have to modify each properties.

            state.status = 'initial';
            state.page = null;
            state.totalPages = null;
            state.size = 20;
            state.results = [];
            state.error = null;
            state.query = '';
        }
    },
    extraReducers: {
        [searchSongs.pending]: (state, action) => {
            state.status = 'updating';
        },

        [searchSongs.fulfilled]: (state, action) => {
            const { page, totalPages, results } = action.payload;
            state.page = page;
            state.totalPages = totalPages;
            state.results = results;
            state.status = 'idle';
        },
        [searchSongs.rejected]: (state, action) => {
            state.status = 'error';
            state.error = action.payload.error
        }
    }
});

export const { updateQueryString, updateStatus, updatePage, resetToInitial } = siteState.actions;

export default siteState.reducer;