import os, sqlite3, time
from elasticsearch import Elasticsearch, exceptions
from elasticsearch.helpers import streaming_bulk

# from elasticsearch.serializer import JSONSerializer

host = os.getenv("ELASTIC_HOST")
port = os.getenv("ELASTIC_PORT")
sleep_time = os.getenv("SLEEP_TIME")


def connect():
    while True:
        try:
            time.sleep(float(sleep_time))
            es = Elasticsearch([{"host": host, "port": port}])

            conn = sqlite3.connect("subset_track_metadata.db")
            c = conn.cursor()
            return es, conn, c
        except exceptions.ConnectionError:
            continue


def gendata():
    for row in c.execute(
        "select track_id, artist_name, title, duration, release from songs"
    ):
        yield {
            "_id": row[0],
            "artist": row[1],
            "title": row[2],
            "length": row[3],
            "album": row[4],
        }


es, conn, c = connect()
counter = 0

if not es.indices.exists("songs"):
    for ok, action in streaming_bulk(client=es, index="songs", actions=gendata()):
        counter += ok
        print(f"Indexed {counter} documents")
