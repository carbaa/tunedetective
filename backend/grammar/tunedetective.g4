grammar tunedetective;

// TODO: fix token not seperated by space getting included

query: WS* token (WS+ token)* EOF;
token: option # TokenOption | regular_term # TokenRegularTerm;

regular_term: EXACT_PHRASE | NOT_WS+;
numeric_term: DIGIT+ ('.' DIGIT+)?;

option: match_option;

match_option:
	match_key = regular_key ':' WS* match_value = regular_term # MatchOptionRegular;

regular_key: ARTIST | ALBUM | TITLE;

//------------------------Lexer Rules------------------------

ARTIST: 'artist';
ALBUM: 'album';
TITLE: 'title';

DIGIT: [0123456789];
WS: [\t\r\n ];
NOT_WS: ~[\t\r\n ];

EXACT_PHRASE: '"' .* '"';
