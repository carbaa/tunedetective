# Generated from tunedetective.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\13")
        buf.write("L\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\3\2\7\2\22\n\2\f\2\16\2\25\13\2\3\2\3\2\6\2\31\n")
        buf.write("\2\r\2\16\2\32\3\2\7\2\36\n\2\f\2\16\2!\13\2\3\2\3\2\3")
        buf.write("\3\3\3\5\3\'\n\3\3\4\3\4\6\4+\n\4\r\4\16\4,\5\4/\n\4\3")
        buf.write("\5\6\5\62\n\5\r\5\16\5\63\3\5\3\5\6\58\n\5\r\5\16\59\5")
        buf.write("\5<\n\5\3\6\3\6\3\7\3\7\3\7\7\7C\n\7\f\7\16\7F\13\7\3")
        buf.write("\7\3\7\3\b\3\b\3\b\2\2\t\2\4\6\b\n\f\16\2\3\3\2\5\7\2")
        buf.write("N\2\23\3\2\2\2\4&\3\2\2\2\6.\3\2\2\2\b\61\3\2\2\2\n=\3")
        buf.write("\2\2\2\f?\3\2\2\2\16I\3\2\2\2\20\22\7\t\2\2\21\20\3\2")
        buf.write("\2\2\22\25\3\2\2\2\23\21\3\2\2\2\23\24\3\2\2\2\24\26\3")
        buf.write("\2\2\2\25\23\3\2\2\2\26\37\5\4\3\2\27\31\7\t\2\2\30\27")
        buf.write("\3\2\2\2\31\32\3\2\2\2\32\30\3\2\2\2\32\33\3\2\2\2\33")
        buf.write("\34\3\2\2\2\34\36\5\4\3\2\35\30\3\2\2\2\36!\3\2\2\2\37")
        buf.write("\35\3\2\2\2\37 \3\2\2\2 \"\3\2\2\2!\37\3\2\2\2\"#\7\2")
        buf.write("\2\3#\3\3\2\2\2$\'\5\n\6\2%\'\5\6\4\2&$\3\2\2\2&%\3\2")
        buf.write("\2\2\'\5\3\2\2\2(/\7\13\2\2)+\7\n\2\2*)\3\2\2\2+,\3\2")
        buf.write("\2\2,*\3\2\2\2,-\3\2\2\2-/\3\2\2\2.(\3\2\2\2.*\3\2\2\2")
        buf.write("/\7\3\2\2\2\60\62\7\b\2\2\61\60\3\2\2\2\62\63\3\2\2\2")
        buf.write("\63\61\3\2\2\2\63\64\3\2\2\2\64;\3\2\2\2\65\67\7\3\2\2")
        buf.write("\668\7\b\2\2\67\66\3\2\2\289\3\2\2\29\67\3\2\2\29:\3\2")
        buf.write("\2\2:<\3\2\2\2;\65\3\2\2\2;<\3\2\2\2<\t\3\2\2\2=>\5\f")
        buf.write("\7\2>\13\3\2\2\2?@\5\16\b\2@D\7\4\2\2AC\7\t\2\2BA\3\2")
        buf.write("\2\2CF\3\2\2\2DB\3\2\2\2DE\3\2\2\2EG\3\2\2\2FD\3\2\2\2")
        buf.write("GH\5\6\4\2H\r\3\2\2\2IJ\t\2\2\2J\17\3\2\2\2\f\23\32\37")
        buf.write("&,.\639;D")
        return buf.getvalue()


class tunedetectiveParser ( Parser ):

    grammarFileName = "tunedetective.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'.'", "':'", "'artist'", "'album'", "'title'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "ARTIST", "ALBUM", 
                      "TITLE", "DIGIT", "WS", "NOT_WS", "EXACT_PHRASE" ]

    RULE_query = 0
    RULE_token = 1
    RULE_regular_term = 2
    RULE_numeric_term = 3
    RULE_option = 4
    RULE_match_option = 5
    RULE_regular_key = 6

    ruleNames =  [ "query", "token", "regular_term", "numeric_term", "option", 
                   "match_option", "regular_key" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    ARTIST=3
    ALBUM=4
    TITLE=5
    DIGIT=6
    WS=7
    NOT_WS=8
    EXACT_PHRASE=9

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class QueryContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def token(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(tunedetectiveParser.TokenContext)
            else:
                return self.getTypedRuleContext(tunedetectiveParser.TokenContext,i)


        def EOF(self):
            return self.getToken(tunedetectiveParser.EOF, 0)

        def WS(self, i:int=None):
            if i is None:
                return self.getTokens(tunedetectiveParser.WS)
            else:
                return self.getToken(tunedetectiveParser.WS, i)

        def getRuleIndex(self):
            return tunedetectiveParser.RULE_query

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterQuery" ):
                listener.enterQuery(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitQuery" ):
                listener.exitQuery(self)




    def query(self):

        localctx = tunedetectiveParser.QueryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_query)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 17
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==tunedetectiveParser.WS:
                self.state = 14
                self.match(tunedetectiveParser.WS)
                self.state = 19
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 20
            self.token()
            self.state = 29
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==tunedetectiveParser.WS:
                self.state = 22 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 21
                    self.match(tunedetectiveParser.WS)
                    self.state = 24 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==tunedetectiveParser.WS):
                        break

                self.state = 26
                self.token()
                self.state = 31
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 32
            self.match(tunedetectiveParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TokenContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return tunedetectiveParser.RULE_token

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class TokenRegularTermContext(TokenContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a tunedetectiveParser.TokenContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def regular_term(self):
            return self.getTypedRuleContext(tunedetectiveParser.Regular_termContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTokenRegularTerm" ):
                listener.enterTokenRegularTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTokenRegularTerm" ):
                listener.exitTokenRegularTerm(self)


    class TokenOptionContext(TokenContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a tunedetectiveParser.TokenContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def option(self):
            return self.getTypedRuleContext(tunedetectiveParser.OptionContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTokenOption" ):
                listener.enterTokenOption(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTokenOption" ):
                listener.exitTokenOption(self)



    def token(self):

        localctx = tunedetectiveParser.TokenContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_token)
        try:
            self.state = 36
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [tunedetectiveParser.ARTIST, tunedetectiveParser.ALBUM, tunedetectiveParser.TITLE]:
                localctx = tunedetectiveParser.TokenOptionContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 34
                self.option()
                pass
            elif token in [tunedetectiveParser.NOT_WS, tunedetectiveParser.EXACT_PHRASE]:
                localctx = tunedetectiveParser.TokenRegularTermContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 35
                self.regular_term()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Regular_termContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EXACT_PHRASE(self):
            return self.getToken(tunedetectiveParser.EXACT_PHRASE, 0)

        def NOT_WS(self, i:int=None):
            if i is None:
                return self.getTokens(tunedetectiveParser.NOT_WS)
            else:
                return self.getToken(tunedetectiveParser.NOT_WS, i)

        def getRuleIndex(self):
            return tunedetectiveParser.RULE_regular_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRegular_term" ):
                listener.enterRegular_term(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRegular_term" ):
                listener.exitRegular_term(self)




    def regular_term(self):

        localctx = tunedetectiveParser.Regular_termContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_regular_term)
        self._la = 0 # Token type
        try:
            self.state = 44
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [tunedetectiveParser.EXACT_PHRASE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 38
                self.match(tunedetectiveParser.EXACT_PHRASE)
                pass
            elif token in [tunedetectiveParser.NOT_WS]:
                self.enterOuterAlt(localctx, 2)
                self.state = 40 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 39
                    self.match(tunedetectiveParser.NOT_WS)
                    self.state = 42 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==tunedetectiveParser.NOT_WS):
                        break

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Numeric_termContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DIGIT(self, i:int=None):
            if i is None:
                return self.getTokens(tunedetectiveParser.DIGIT)
            else:
                return self.getToken(tunedetectiveParser.DIGIT, i)

        def getRuleIndex(self):
            return tunedetectiveParser.RULE_numeric_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumeric_term" ):
                listener.enterNumeric_term(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumeric_term" ):
                listener.exitNumeric_term(self)




    def numeric_term(self):

        localctx = tunedetectiveParser.Numeric_termContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_numeric_term)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 47 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 46
                self.match(tunedetectiveParser.DIGIT)
                self.state = 49 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==tunedetectiveParser.DIGIT):
                    break

            self.state = 57
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==tunedetectiveParser.T__0:
                self.state = 51
                self.match(tunedetectiveParser.T__0)
                self.state = 53 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 52
                    self.match(tunedetectiveParser.DIGIT)
                    self.state = 55 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==tunedetectiveParser.DIGIT):
                        break



        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OptionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def match_option(self):
            return self.getTypedRuleContext(tunedetectiveParser.Match_optionContext,0)


        def getRuleIndex(self):
            return tunedetectiveParser.RULE_option

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOption" ):
                listener.enterOption(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOption" ):
                listener.exitOption(self)




    def option(self):

        localctx = tunedetectiveParser.OptionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_option)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 59
            self.match_option()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Match_optionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return tunedetectiveParser.RULE_match_option

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class MatchOptionRegularContext(Match_optionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a tunedetectiveParser.Match_optionContext
            super().__init__(parser)
            self.match_key = None # Regular_keyContext
            self.match_value = None # Regular_termContext
            self.copyFrom(ctx)

        def regular_key(self):
            return self.getTypedRuleContext(tunedetectiveParser.Regular_keyContext,0)

        def regular_term(self):
            return self.getTypedRuleContext(tunedetectiveParser.Regular_termContext,0)

        def WS(self, i:int=None):
            if i is None:
                return self.getTokens(tunedetectiveParser.WS)
            else:
                return self.getToken(tunedetectiveParser.WS, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMatchOptionRegular" ):
                listener.enterMatchOptionRegular(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMatchOptionRegular" ):
                listener.exitMatchOptionRegular(self)



    def match_option(self):

        localctx = tunedetectiveParser.Match_optionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_match_option)
        self._la = 0 # Token type
        try:
            localctx = tunedetectiveParser.MatchOptionRegularContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 61
            localctx.match_key = self.regular_key()
            self.state = 62
            self.match(tunedetectiveParser.T__1)
            self.state = 66
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==tunedetectiveParser.WS:
                self.state = 63
                self.match(tunedetectiveParser.WS)
                self.state = 68
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 69
            localctx.match_value = self.regular_term()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Regular_keyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ARTIST(self):
            return self.getToken(tunedetectiveParser.ARTIST, 0)

        def ALBUM(self):
            return self.getToken(tunedetectiveParser.ALBUM, 0)

        def TITLE(self):
            return self.getToken(tunedetectiveParser.TITLE, 0)

        def getRuleIndex(self):
            return tunedetectiveParser.RULE_regular_key

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRegular_key" ):
                listener.enterRegular_key(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRegular_key" ):
                listener.exitRegular_key(self)




    def regular_key(self):

        localctx = tunedetectiveParser.Regular_keyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_regular_key)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 71
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << tunedetectiveParser.ARTIST) | (1 << tunedetectiveParser.ALBUM) | (1 << tunedetectiveParser.TITLE))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





