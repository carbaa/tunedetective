from elasticsearch import Elasticsearch
from myparser import MyParser

from flask import Flask, request, make_response
import json, os

app = Flask(__name__)

elastic_host = os.getenv("ELASTIC_HOST")
elastic_port = os.getenv("ELASTIC_PORT")

es = Elasticsearch([{"host": elastic_host, "port": elastic_port}])

parser = MyParser()
config = {"size": 20, "from": "0"}

# TODO: return error on query with invalid syntax


@app.route("/api", methods=["GET", "POST", "OPTIONS"])
def api():
    res = make_response()
    res.headers.add("Access-Control-Allow-Origin", "*")
    res.headers.add("Access-Control-Allow-Headers", "*")

    if request.method == "OPTIONS":
        return res

    elif request.method == "POST":
        if not request.is_json:
            res.set_data(json.dumps({"error": "json only"}))
            return res

        requestBody = request.get_json()

        if "query" not in requestBody:
            res.set_data(json.dumps({"error": "please supply a query"}))
            return res

        size = requestBody.get("size", config["size"])
        startFrom = requestBody.get("from", config["from"])
        query = requestBody["query"]

        queryBody = parser.generateQuery(query)
        queryParams = {"size": size, "from": startFrom}
        try:
            searchResult = es.search(body=queryBody, params=queryParams)
        except:
            res.set_data(json.dumps({"error": "connection error"}))
            return res

        total = searchResult["hits"]["total"]["value"]
        docArray = list(map(lambda doc: doc["_source"], searchResult["hits"]["hits"]))
        res.set_data(
            json.dumps(
                {
                    "total": total,
                    "from": startFrom,
                    "size": size,
                    "results": docArray,
                }
            )
        )

        return res
    else:
        res = make_response({"error": "please use POST method"}, 400)
        return res