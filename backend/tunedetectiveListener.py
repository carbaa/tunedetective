# Generated from tunedetective.g4 by ANTLR 4.8
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .tunedetectiveParser import tunedetectiveParser
else:
    from tunedetectiveParser import tunedetectiveParser

# This class defines a complete listener for a parse tree produced by tunedetectiveParser.
class tunedetectiveListener(ParseTreeListener):

    # Enter a parse tree produced by tunedetectiveParser#query.
    def enterQuery(self, ctx:tunedetectiveParser.QueryContext):
        pass

    # Exit a parse tree produced by tunedetectiveParser#query.
    def exitQuery(self, ctx:tunedetectiveParser.QueryContext):
        pass


    # Enter a parse tree produced by tunedetectiveParser#TokenOption.
    def enterTokenOption(self, ctx:tunedetectiveParser.TokenOptionContext):
        pass

    # Exit a parse tree produced by tunedetectiveParser#TokenOption.
    def exitTokenOption(self, ctx:tunedetectiveParser.TokenOptionContext):
        pass


    # Enter a parse tree produced by tunedetectiveParser#TokenRegularTerm.
    def enterTokenRegularTerm(self, ctx:tunedetectiveParser.TokenRegularTermContext):
        pass

    # Exit a parse tree produced by tunedetectiveParser#TokenRegularTerm.
    def exitTokenRegularTerm(self, ctx:tunedetectiveParser.TokenRegularTermContext):
        pass


    # Enter a parse tree produced by tunedetectiveParser#regular_term.
    def enterRegular_term(self, ctx:tunedetectiveParser.Regular_termContext):
        pass

    # Exit a parse tree produced by tunedetectiveParser#regular_term.
    def exitRegular_term(self, ctx:tunedetectiveParser.Regular_termContext):
        pass


    # Enter a parse tree produced by tunedetectiveParser#numeric_term.
    def enterNumeric_term(self, ctx:tunedetectiveParser.Numeric_termContext):
        pass

    # Exit a parse tree produced by tunedetectiveParser#numeric_term.
    def exitNumeric_term(self, ctx:tunedetectiveParser.Numeric_termContext):
        pass


    # Enter a parse tree produced by tunedetectiveParser#option.
    def enterOption(self, ctx:tunedetectiveParser.OptionContext):
        pass

    # Exit a parse tree produced by tunedetectiveParser#option.
    def exitOption(self, ctx:tunedetectiveParser.OptionContext):
        pass


    # Enter a parse tree produced by tunedetectiveParser#MatchOptionRegular.
    def enterMatchOptionRegular(self, ctx:tunedetectiveParser.MatchOptionRegularContext):
        pass

    # Exit a parse tree produced by tunedetectiveParser#MatchOptionRegular.
    def exitMatchOptionRegular(self, ctx:tunedetectiveParser.MatchOptionRegularContext):
        pass


    # Enter a parse tree produced by tunedetectiveParser#regular_key.
    def enterRegular_key(self, ctx:tunedetectiveParser.Regular_keyContext):
        pass

    # Exit a parse tree produced by tunedetectiveParser#regular_key.
    def exitRegular_key(self, ctx:tunedetectiveParser.Regular_keyContext):
        pass



del tunedetectiveParser