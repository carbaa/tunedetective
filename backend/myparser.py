import sys
from antlr4 import InputStream, CommonTokenStream, ParseTreeWalker
from tunedetectiveLexer import tunedetectiveLexer
from tunedetectiveParser import tunedetectiveParser
from tunedetectiveListener import tunedetectiveListener


class QueryBuilder:
    def __init__(self):
        self.queryArray = []

        # maps field to elasticsearch fields
        self.fieldMap = {
            # common for all charts
            "artist": "artist",
            "title": "title",
            "album": "album",
        }

    def addMatch(self, field: str, query: str):
        if field:
            elasticSearchField = self.fieldMap[field.lower()]
            self.queryArray.append({"match": {elasticSearchField: query}})
        else:
            self.queryArray.append({"multi_match": {"query": query, "fields": []}})

    def buildQuery(self):
        return {"query": {"bool": {"must": self.queryArray}}}


class MyTuneDetectiveListener(tunedetectiveListener):
    def __init__(self):
        self.qb = QueryBuilder()

    def enterTokenRegularTerm(self, ctx):
        query = ctx.getText()
        self.qb.addMatch("", query)

    def enterMatchOptionRegular(self, ctx):
        field = ctx.match_key.getText()
        query = ctx.match_value.getText()
        self.qb.addMatch(field, query)


class MyParser:
    def generateQuery(self, queryString):
        input_stream = InputStream(queryString)

        lexer = tunedetectiveLexer(input_stream)
        stream = CommonTokenStream(lexer)
        parser = tunedetectiveParser(stream)
        tree = parser.query()

        listener = MyTuneDetectiveListener()
        walker = ParseTreeWalker()
        walker.walk(listener, tree)
        return listener.qb.buildQuery()