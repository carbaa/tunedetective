# TuneDetective
A WIP personal song search engine based on Elasticsearch, Flask, React, Material-UI and more

## Demo

Try the demo [here](https://carbaa.gitlab.io/tunedetective)

## Getting Started

### Backend:

The following dependencies are required:

- antlr4-python3-runtime
- elasticsearch
- flask
- gunicorn

There's a `docker-compose.yml` file that you can use to build and run the http
version of the backend. This will also load a subset of [Million Song Dataset](http://millionsongdataset.com/)

### Frontend:

The following variable must be set to the address of the Flask API or else it will not be able to fetch from the backend:

- API_ADDRESS

To run, follow these instructions:

- `npm install` 
- `npm run start:dev` 

To build, run `npm build`